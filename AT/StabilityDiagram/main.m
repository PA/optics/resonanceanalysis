%% Script for nonlinear resonances

sext = true;
oct = true;
delta = 0;

%% Lattice to be analysed

load('../Lattices/ESRF_standard_cell.mat')
ring = ARCA;% ring = rmin;
%% Sextupolar resonances

if sext
sext_resonances(ring, delta)
end

if oct
    
end