function sext_resonances(ring, Denergy, varargin)
%
% Functions which calculates and plots the transverse limitations 
% of sextupolar resonances on the dynamic aperture at a given 
% energy deviation.
%
% Following the paper "Nonlinear dynamics with sextupoles in 
% low-emittance light source storage rings", R. Nagaoka, K. Yoshida and 
% M. Hara, 1991
%
%
% - Resonances of the type Qx = M to be confirmed 
% - To be augmented with the inclusion of the energy deviation component.
% - Higher-order to be included
%
%
% Inputs : - ring, lattice to be analysed, can full or parts
%          - Denergy, energy deviation
%
% Optional outputs : - orders, stating the order of sextupole resonance
% required. DEFAULT = linear resonances 1,3.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% if nargin <3
%     order = [1;3];
% end
% 
% l = length(order);
% 
% if l > 2
%     fprintf('Please make sure the order is in {1;3}')
%     return
% end

if (Denergy~=0)
fprintf('Off-energy not yet supported')
return
end
    
    
%% Get the position of the sextupoles and main parameters

RING1 = ring;
indice = find(atgetcells(RING1, 'Class', 'Sextupole'));
n_sext = length(indice);
lsext = atgetfieldvalues(RING1(indice), 'Length');
k2l = atgetfieldvalues(RING1(indice), 'PolynomB', {3}).*lsext;


[lindata,tune,chrom]=atlinopt(RING1,0,1:length(RING1)+1);
beta=cat(1,lindata.beta);
muxy=cat(1,lindata.mu);
betax=beta(:,1);
betay=beta(:,2);
betax0 = betax(1);
betay0 = betay(1);

% Averaged twiss functions to be closer to the thin lens approximation
[lindata,avebeta,avemu,avedisp,nu,xsi]=atavedata(RING1,0,1:length(RING1)+1);
beta = avebeta;
disp = avedisp;
muxy = avemu;
betax=beta(:,1);
betay=beta(:,2);
alpha=cat(1,lindata.alpha);
alphax=alpha(:,1);
alphay=alpha(:,2);
nux=muxy(length(RING1)+1,1)/2/pi;
nuy=muxy(length(RING1)+1,2)/2/pi;
phix=muxy(:,1);
phiy=muxy(:,2);

SPos=cat(1,lindata.SPos);
s=SPos(:,1);
disp= cat(2,lindata.Dispersion);

dispx=disp(1,:)';
Ddispx=disp(2,:)';

Qx = nux
Qy = nuy

N = 3*floor(Qx)*10;

phip = phix+2*phiy;
phim = phix-2*phiy;
Qp = Qx+2*Qy;
Qm = Qx-2*Qy;

%% Coefficient calculations
theta = 2*pi*(s(indice)+lsext/2)/s(length(RING1)+1);
R = s(length(RING1)+1)/2*pi;
% M =linspace(-N,N,2*N+1);

A3M = zeros(1, N);
A1M = zeros(1, N);
B1M = zeros(1, N);

for j=1:N
	for k=1:n_sext
	Sk = betax(indice(k))^(3/2)*k2l(k);
	Skbar = betax(indice(k))^(1/2)* betay(indice(k))*k2l(k);
    A3M(j) = A3M(j) + (1/48/pi)*(Sk*(cos(3*phix(indice(k))-Qx*3*theta(k)+j*theta(k))));
    A1M(j) = A1M(j) + (1/48/pi)*(Sk*(cos(phix(indice(k))-Qx*theta(k)+j*theta(k))));
    B1M(j) = B1M(j) + (1/48/pi)*(Skbar*(cos(phix(indice(k))-Qx*theta(k)+j*theta(k))));
	end
end


%% Plots
a = 150;
b = 150;
Nt = 10;
[xx,zz]=atdynap(RING1,Nt, Denergy, 0.005);

%Scaling the axis...
xmin = min(xx);
xmax= max(xx);
zmax = max(zz);

figure(1)
hold on
set(gcf,'color','w')
set(gca,'fontsize',20');
plot(xx*1000,zz*1000,'r','LineWidth', 3.0, 'marker','.')
xlabel('x (mm)')
ylabel('y (mm)')
axis([-b b 0 a])
%% Resonances of type 3Qx = M

for k=1:N
delta = Qx-k/3;
xa_l_m= delta/A3M(k)/6*sqrt(betax0);   %left boundary
xa_r_m= -2*xa_l_m;    %right boundary
   if(xa_l_m > -b/10^3  &&  xa_l_m < b/10^3 )
       x(1,1) = xa_l_m*10^3; y(1,1) = 0;
       x(1,2) = xa_l_m*10^3; y(1,2) = a; 
     
       plot(x,y, 'g')
       str = ['(3,', num2str(k),')'];
       text(xa_l_m*10^3, a*rand, str, 'FontSize',10,'FontWeight','bold', 'Color', [0, 0, 0]);
   end
   if (xa_r_m > -b/10^3  &&  xa_r_m < b/10^3 )
      x(1,1) = xa_r_m*10^3; y(1,1) = 0;
       x(1,2) = xa_r_m*10^3; y(1,2) = a; 
     
       plot(x,y, 'g')
       str = ['(3,', num2str(k),')'];
       text(xa_r_m*10^3, a*rand, str, 'FontSize',10,'FontWeight','bold', 'Color', [0, 0, 0]);  
   end   
end
%% Resonances of type Qx = M // extracted from CATS
x1st=[];
y1st=[];
for k=1:N
    ystep = 0.001;
    m3 = 3*k;
    if(m3 >= 0 && m3 <= N)
        delta = Qx-k;
        delta2 = delta^2;
        alpha = A3M(k) + 3*A1M(k);
        lambda = A3M(k)-k;
        ymm = ystep;
        ic  = 1;
        while (ymm < a/1000)
            ajy = ymm*ymm/(2.0*betay0);
            FF  = 144.0*alpha*B1M(k)*ajy/delta2;
            if(FF > -1.0)
                FF = sqrt(1.0 + FF);
                xamm =-delta*(1.0 + FF)/(6.0*alpha)*sqrt(betax0);
                xbmm = delta*(2.0*FF - 1.0)/(6.0*alpha)*sqrt(betax0);
                if(xamm < b/10^3 && xamm > -b/10^3)
                    x1st(1,ic) = xamm;
                    y1st(1,ic) = ymm;
                    ic = ic + 1;
                end
                if(xbmm < b/10^3 && xbmm > -b/10^3)
                    x1st(1,ic) = xbmm;
                    y1st(1,ic) = ymm;
                    ic = ic + 1;
                end
            end
            ymm = ymm + ystep;
        end
        if(ic > 1)
            %        Re-ordering of the data:
            Npts3 = ic-1;
            ic1   = 0;
            for ic1=1:Npts3
                xxmin = b/10^3;
                for l=1:Npts3
                    if(ic1 == 1)
                        if(x1st(1,l) < xxmin)
                            xxmin = x1st(1,l);
                            map1st(1,ic1) = l;
                        end
                    else
                        if(x1st(1,l) < xxmin  &&  x1st(1,l) > x1st(1,map1st(1,ic1-1)))
                            xxmin = x1st(1,l);
                            map1st(1,ic1) = l;
                        end
                    end
                end
                x1stN(1,ic1) = x1st(1,map1st(1,ic1));
                y1stN(1,ic1) = y1st(1,map1st(1,ic1));
            end
            
            plot(x1stN(1,1:ic-1)*1000,y1stN(1,1:ic-1)*1000,'Color',[0.8,0.1,0.1],'LineWidth',2.5);
            ResLab=['[1,',num2str(k),']'];
            ic1pos = round((ic1-1+1)/2) + round((ic1-1-1)/3.0*(2*rand-1.0)/3.0);
            text(x1stN(1,ic1pos)*1000,y1stN(1,ic1pos)*1000,...
                texlabel(ResLab,'literal'),'FontSize',14,'FontWeight','bold','Color',[0.8,0.1,0.1])
        end
        
    end
    
end


%% Output files

fid = fopen('resonances_sextupoles_out.txt','w');
fprintf(fid,'%s\n','Betatron tune (Qx, Qy)');
fprintf(fid,'%i\t %i%i\n',Qx);
fprintf(fid,'%i\n\n',Qy);
fprintf(fid,'%s\n','Initial Twiss and dispersion alphax betax alphay betay disp0 disp0p');
fprintf(fid,'%i\t %i\t %i\t %i\n',alphax(1), betax0, alphay(1), betay0);
fprintf(fid,'%i\t %i\n\n',disp(1,1), disp(2,1));
fprintf(fid,'%s\n','Nonlinear magnets list');
fprintf(fid,'%s\n','Sextupoles');
fprintf(fid,'%s\n','Index Phix/2pi Phiy/2pi SK SKB s/R');
for k=1:n_sext
SK = betax(indice(k))^(3/2)*k2l(k);
SKB = betax(indice(k))^(1/2)* betay(indice(k))*k2l(k);
fprintf(fid,'%i\t %i\t %i\t %i\t %i\t %i\n\n', 3, phix(indice(k)), phiy(indice(k)), SK, SKB, s(indice(k))/R);
end
fclose(fid)


